import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductS1Component } from './product-s1.component';

describe('ProductS1Component', () => {
  let component: ProductS1Component;
  let fixture: ComponentFixture<ProductS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
