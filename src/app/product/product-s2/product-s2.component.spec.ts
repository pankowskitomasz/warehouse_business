import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductS2Component } from './product-s2.component';

describe('ProductS2Component', () => {
  let component: ProductS2Component;
  let fixture: ComponentFixture<ProductS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
