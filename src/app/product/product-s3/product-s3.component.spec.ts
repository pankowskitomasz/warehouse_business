import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductS3Component } from './product-s3.component';

describe('ProductS3Component', () => {
  let component: ProductS3Component;
  let fixture: ComponentFixture<ProductS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
