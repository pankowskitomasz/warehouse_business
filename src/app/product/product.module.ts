import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product/product.component';
import { ProductS1Component } from './product-s1/product-s1.component';
import { ProductS2Component } from './product-s2/product-s2.component';
import { ProductS3Component } from './product-s3/product-s3.component';


@NgModule({
  declarations: [
    ProductComponent,
    ProductS1Component,
    ProductS2Component,
    ProductS3Component
  ],
  imports: [
    CommonModule,
    ProductRoutingModule
  ]
})
export class ProductModule { }
